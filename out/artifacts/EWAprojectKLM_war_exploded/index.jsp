<%--
  Created by IntelliJ IDEA.
  User: mark_
  Date: 9-10-2017
  Time: 17:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/style.css"></link>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="https://w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<header>
    <div class="logo w3-mobile">
        <img class="w3-mobile" src="img/klmlogo.jpg">
    </div>
    <div id="google_translate_element"></div>
    <script type="text/javascript">
        function googleTranslateElementInit() {
            new google.translate.TranslateElement({
                pageLanguage: 'en',
                includedLanguages: 'de,en,es,fr,nl,tr,zh-CN',
                layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
                autoDisplay: false
            }, 'google_translate_element');
        }
    </script>
    <script type="text/javascript"
            src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</header>

<div class="container fadeInDown animated w3-mobile">
    <img src="img/menwomanicon.png">

    <form action="/index" method="post">
        <div class="form-input">
            <input type="text" name="username"
                   placeholder="Enter Username">
        </div>
        <div class="form-input">
            <input type="password" name="password"
                   placeholder="Enter Password">
        </div>
        <input type="submit" name="submit"
               value="Login" class="btn-login">
    </form>
    <strong style="color:red;">${errorMessage}</strong>
</div>

<div class="footer w3-padding-xlarge w3-center">
    <p> Powered by: KLM TEAM 4 </p>
</div>


</body>
</html>
