<%--
  Created by IntelliJ IDEA.
  User: mark_
  Date: 10-10-2017
  Time: 10:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome</title>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 75%;
            width: 75%;
        }

        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
<div id="map"></div>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    var map;
    var oldZoom = null;
    var oldCenter = null;
    var infoWindow;
    var canRegisterTip;
    var shouldDisplayTips;

    function saveTip(lat, lng) {
        $.post("/Homepage", {ACTION: "REGISTER_TIP", lat: lat, lng: lng})
            .done(function () {
                alert("Tip toegevoegd!");
            }).fail(function () {
            alert("Tip kon niet worden toegevoegd");
        });
    }

    function ToggleRegistering(button) {
        if (canRegisterTip) {
            canRegisterTip = false;
            button.value = "Tip registratie inschakelen";
        } else {
            canRegisterTip = true;
            button.value = "Tip registratie uitschakelen";
        }
    }

    function toggleTipDisplaying(button) {
        if (shouldDisplayTips) {
            shouldDisplayTips = false;
            button.value = "Tips weergeven";
        } else {
            shouldDisplayTips = true;
            button.value = "Tips verbergen";
        }
    }

    function showTips() {

    }

    function hideTips() {

    }

    function getTips(lat, lng) {
        $.get("/Homepage", {ACTION: "GET_TIPS", lat: lat, lng: lng})
            .done(function (data) {
                alert("tips");
//                alert(JSON.parse(data));
            });
    }

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -34.397, lng: 150.644},
            zoom: 18
        });

        map.addListener('bounds_changed', function () {
        });

        map.addListener('idle', function () {
            getTips(map.getCenter().lat, map.getCenter().lng);
        });

        infoWindow = new google.maps.InfoWindow;

        google.maps.event.addListener(map, 'click', function (event) {
            if (!canRegisterTip)
                return;

            if (confirm('Tip registreren?')) {
//                alert( "Latitude: "+event.latLng.lat()+" "+", longitude: "+event.latLng.lng() );
                saveTip(event.latLng.lat(), event.latLng.lng());
            }
        });

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                infoWindow.setPosition(pos);
                infoWindow.setContent('Location found.');
                infoWindow.open(map);
                map.setCenter(pos);
            }, function () {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation
            ? 'Error: The Geolocation service failed.'
            : 'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
    }
</script>
<input id="registerToggleButton" type="button" value="Tip registratie inschakelen" onclick="ToggleRegistering(this);"/>
<input id="displayTipToggleButton" type="button" value="Tips weergeven" onclick="toggleTipDisplaying(this);"/>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHiRZ0CvsY1blyjvkJaKYv6FskZqmB-yQ&callback=initMap"
        async defer></script>
<%--<h1>WELCOME</h1>--%>
<%--<p> Dear: ${username}</p>--%>
<%--<p> Your password is: ${password}</p>--%>

</body>
</html>
