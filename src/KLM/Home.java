package KLM;

import com.google.gson.Gson;
import dataLayer.DB_API;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "Homepage")
public class Home extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("ACTION");

        switch (action) {
            case "REGISTER_TIP":
                if (request.getParameter("lat") == null || request.getParameter("lng") == null) {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }

                double latitude = Double.parseDouble(request.getParameter("lat"));
                double longitude = Double.parseDouble(request.getParameter("lng"));

                try {
                    if (DB_API.registerTip(latitude, longitude, appLayer.UserManager.getInstance().getUser().getId())) {
                        response.setStatus(HttpServletResponse.SC_OK);
                    } else {
                        response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("ACTION");

        switch (action) {
            case "GET_TIPS":
                if (request.getParameter("lat") == null || request.getParameter("lng") == null) {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }

                double latitude = Double.parseDouble(request.getParameter("lat"));
                double longitude = Double.parseDouble(request.getParameter("lng"));

                List retrievedTips = null;

                try {
//                    retrievedTips = DB_API.getRelativeTips(latitude, longitude);
                    retrievedTips = DB_API.getAllTips();
                } catch (SQLException e) {
                    response.sendError(HttpServletResponse.SC_BAD_GATEWAY);
                } catch (ClassNotFoundException e) {
                    response.sendError(HttpServletResponse.SC_BAD_GATEWAY);
                }

                System.out.println(retrievedTips.size());
                if (retrievedTips.size() == 0)
                    return;

                String json = new Gson().toJson(retrievedTips);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
        }
    }
}
