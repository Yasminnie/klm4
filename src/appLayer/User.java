package appLayer;

import javax.persistence.Entity;

@Entity
public class User extends appLayer.Entity {

    private String userName;
    private String occupation;
    private String name;

    public User(String userName, String occupation) {
        this.userName = userName;
        this.occupation = occupation;
    }

    public User() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}