package appLayer;

public class TipBoundsWrapper {

    private double neLat;
    private double neLng;
    private double swLat;
    private double swLng;


    public TipBoundsWrapper() {
    }

    public void setNeLat(double neLat) {
        this.neLat = neLat;
    }

    public double getNeLat() {
        return neLat;
    }

    public void setNeLng(double neLng) {
        this.neLng = neLng;
    }

    public double getNeLng() {
        return neLng;
    }

    public void setSwLat(double swLat) {
        this.swLat = swLat;
    }

    public double getSwLat() {
        return swLat;
    }

    public void setSwLng(double swLng) {
        this.swLng = swLng;
    }

    public double getSwLng() {
        return swLng;
    }
}
