package appLayer;

import javax.servlet.annotation.WebServlet;

@WebServlet(name = "UserPageController")

public class UserManager {

    private User user;
    private static UserManager instance;

    public static UserManager getInstance() {
        if (instance == null) {
            instance = new UserManager();
        }
        return instance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
