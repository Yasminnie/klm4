package appLayer;

public class TipReaction extends Entity {

    public int userID;
    public String reaction;

    public TipReaction(String reaction) {
        this.reaction = reaction;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
