package dataLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBContext {

    static final String DB_URL = "jdbc:mysql://localhost:3306/crewtips_db?autoReconnect=true&useSSL=false";
    static final String USER = "root";
    static final String PASS = "root";

    private static Connection connection;

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        if (connection == null) {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        }

        return connection;
    }
}
