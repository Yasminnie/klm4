package dataLayer;

import appLayer.Tip;
import appLayer.User;
import appLayer.UserManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DB_API {

    private static String QUERY_LOG_IN = "SELECT * FROM users WHERE username = ? AND password = ?";
    private static String QUERY_REGISTER_TIP = "INSERT INTO tips (latitude,longitude,user_id) VALUES (?,?,?)";
    private static String QUERY_GET_RELATIVE_TIPS = "SELECT id, latitude, longitude, ( 3959 * acos( cos( radians(?) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(?) ) + sin( radians(?) ) * sin( radians( lat ) ) ) ) AS distance FROM tips HAVING distance < 25 ORDER BY distance LIMIT 0 , 20";


    public static boolean logIn(String username, String password) throws SQLException, ClassNotFoundException {
        PreparedStatement ps = DBContext.getConnection().prepareStatement(QUERY_LOG_IN);
        ps.setString(1, username);
        ps.setString(2, password);

        ResultSet resultSet = ps.executeQuery();

        if (!resultSet.next())
            return false;

        User user = new User();
        user.setUserName(username);
        user.setId(resultSet.getInt("user_id"));
        user.setOccupation(resultSet.getString("occupation"));
        user.setName(resultSet.getString("name"));
        UserManager.getInstance().setUser(user);

        resultSet.close();

        return true;
    }

    public static boolean registerTip(double lat, double lon, int userId) throws SQLException, ClassNotFoundException {
        PreparedStatement ps = DBContext.getConnection().prepareStatement(QUERY_REGISTER_TIP);
        ps.setDouble(1, lat);
        ps.setDouble(2, lon);
        ps.setInt(3, userId);

        return ps.executeUpdate() > 0;
    }

    public static List getAllTips() throws SQLException, ClassNotFoundException {
        PreparedStatement ps = DBContext.getConnection().prepareStatement("SELECT * FROM tips");

        ResultSet rs = ps.executeQuery();

        List retrievedTips = new ArrayList<Tip>();

        while (rs.next()) {
            Tip tip = new Tip();
            tip.setId(rs.getInt("tip_id"));
            tip.setLatitude(rs.getDouble("latitude"));
            tip.setLongitude(rs.getDouble("longitude"));
            tip.setUserId(rs.getInt("user_id"));

            retrievedTips.add(tip);
        }

        return retrievedTips;
    }

    public static List getRelativeTips(double lat, double lng) throws SQLException, ClassNotFoundException {
        PreparedStatement ps = DBContext.getConnection().prepareStatement(QUERY_GET_RELATIVE_TIPS);
        ps.setDouble(1, lat);
        ps.setDouble(2, lng);
        ps.setDouble(3, lat);

        ResultSet rs = ps.executeQuery();

        List retrievedTips = new ArrayList<Tip>();

        while (rs.next()) {
            Tip tip = new Tip();
            tip.setId(rs.getInt("tip_id"));
            tip.setLatitude(rs.getDouble("latitude"));
            tip.setLongitude(rs.getDouble("longitude"));
            tip.setUserId(rs.getInt("user_id"));

            retrievedTips.add(tip);
        }

        return retrievedTips;
    }
}
